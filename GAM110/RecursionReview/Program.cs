﻿using System;

namespace RecursionReview
{
    class Program
    {
        // Write a recursive function that counts from 0 to 20, skipping by n numbers each time,
        // where n is an integer that is provided to the function.
        
        static void SkipPrintTo20(int n, int startingNumber)
        {
            if (startingNumber > 20)
                return;

            Console.WriteLine(startingNumber);
            SkipPrintTo20(n, startingNumber + n);
        }

        static void SkipPrintTo20(int n)
        {
            SkipPrintTo20(n, 0);
        }

        /*Factorial
            Given an integer, compute and return its factorial, n!
        */

        static int Factorial(int n)
        {
            if (n == 1)
                return 1;

            return n * Factorial(n - 1);
        }

        /*
         * IsPalindrome
        Given a string, return a boolean (true or false) indicating if the string contains a palindrome.
        */
        static bool IsPalindrome(string s)
        {
            if (s.Length <= 1)
                return true;

            s = s.ToLower();
            if (s[0] != s[s.Length - 1])
                return false;

            return IsPalindrome(s.Substring(1, s.Length-2));
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            SkipPrintTo20(3);
            SkipPrintTo20(7);

            Console.WriteLine("Factorial of 6: " + Factorial(6));

            Console.WriteLine("Palindrome? " + IsPalindrome("Racecar"));
            Console.WriteLine("Palindrome? " + IsPalindrome("raceCars"));
            Console.WriteLine("Palindrome? " + IsPalindrome("racedcars"));

            Console.ReadLine();
        }
    }
}
