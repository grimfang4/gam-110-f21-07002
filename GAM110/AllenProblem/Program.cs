﻿using System;

//given a string, replace single spaces after periods with two spaces and break lines
//after every two sentences
//Tools
/*
 * for loop
 * string, int, array
 * 
 */

namespace AllenProblem
{
    class Program
    {
        static void Main(string[] args)
        {
            string thestuff = "The forest nyphms screamed. The monster had found them and was closing in. All is now lost.";
            string copy = "";
            int tracker = 2;
            for (int i = 0; i < thestuff.Length; i++)
            {
                if (tracker == 0)
                {
                    copy += "\n";
                    tracker = 2;
                }
                copy += thestuff[i];
                if (thestuff[i] == '.')
                {
                    copy += "  ";
                    --tracker;
                }

            }
            Console.WriteLine(copy);
        }
    }
}
