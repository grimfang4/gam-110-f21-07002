﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Maze_Game_planning
{
    class Test
    {
        public int myNumber;

        public Test()
        {
            myNumber = 5;
        }

        public Test(int initialNumber)
        {
            myNumber = initialNumber;
        }

        public int GetNumber()
        {
            return myNumber;
        }
    }
}
