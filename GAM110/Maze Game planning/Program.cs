﻿using System;

namespace Maze_Game_planning
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");


            int[,] gridData = new int[15, 10]; // Make a 15x10 block of integers
            gridData[3, 2] = 17; // Access the 2D array
            Console.WriteLine("Dimensions: " + gridData.GetLength(0) + " x " + gridData.GetLength(1));



            char[,] mazeData = {{'#', '#', '#', '#', '#', '#'},
                                {'#', ' ', ' ', ' ', ' ', '#'},
                                {'#', ' ', '#', '#', ' ', '#'},
                                {'#', ' ', '#', ' ', ' ', '#'},
                                {'#', ' ', '#', '#', ' ', '#'},
                                {'#', 'S', '#', 'F', ' ', '#'},
                                {'#', '#', '#', '#', '#', '#'} };
            Console.WriteLine("Dimensions: " + mazeData.GetLength(0) + " x " + mazeData.GetLength(1));

            Console.WriteLine("This should be F: " + mazeData[5, 3]);


            Test test1 = new Test();
            Test test2 = new Test(14);

            Console.WriteLine(test1.myNumber);
            Console.WriteLine(test2.GetNumber());


            Console.ReadLine();
        }
    }
}
