﻿using System;

//Given an array of strings, keep track of a cursor
//position starting at (0,0) and parse the strings for
//instructions of how to move the cursor. 
//E.g. "Up four","right","LEFT THREE"

//Tools:
/* Simplify: Array of strings move a cursor
 * Use text inputs to control a cursor on the screen
 * if / else statements (conditionals)
 * .ToLower()
 * store cursor data in two integers (x,y)
 * Switch Case (Essentially if statements)
 * Console.Read /Write
 */
namespace TimProblem
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] instructions = { "Up four", "right", "LEFT FOUR" };
            int[] cursorPos = new int[2]; //0 = x; 1 = y

            string keep = "";
            string keepsecond = "";
            int position = 0;

            for(int i=0; i < instructions.Length; i++)
            {
                for(int j = 0; j < instructions[i].Length; j++)
                {
                    if (instructions[i][j] != ' ')
                    {
                        keep += instructions[i][j];
                        if(j < instructions[i].Length)
                        {
                            position = j;
                        }
                        else
                        {
                            position = 0;
                        }
                    }
                }
                if (position != 0)
                {
                    keepsecond = instructions[i].Substring(position);
                }

                switch (keep.ToLower())
                {
                    case "up":
                        if (keepsecond.Length > 0)
                            cursorPos[1]++;
                        else
                            switch (keepsecond.ToLower())
                            {
                                case "one":
                                    cursorPos[1]++;
                                    break;
                                case "two":
                                    cursorPos[1] += 2;
                                    break;
                                case "three":
                                    cursorPos[1] += 3;
                                    break;
                                case "four":
                                    cursorPos[1] += 4;
                                    break;
                            }
                        break;
                    case "down":
                        break;
                    case "left":
                        break;
                    case "right":
                        break;
                }
            }
        }
    }
}
