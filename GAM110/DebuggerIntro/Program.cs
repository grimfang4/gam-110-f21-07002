﻿using System;

namespace DebuggerIntro
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int a = 9;
            int b = 2;

            for (int i = 0; i < 10; ++i)
            {
                Console.WriteLine("Hello!");
            }

            b = a;
            b = 4;

            Console.ReadLine();
        }
    }
}
