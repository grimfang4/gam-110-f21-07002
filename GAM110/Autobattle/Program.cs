﻿using System;

/*
 * TODO: Create an abstract Character class that has 
 *       the functions required for this file to work properly.
 *       
 * TODO: Create three (3) derived classes that inherit from Character and 
 *       which override the DoBattleCry() and Act() functions (and any others you deem necessary).
 *       
 * TODO: Share your derived class(es) with some friends and 
 *       implement the static function, GetRandom(), in the Character class.
 *       This function should randomly return a new object chosen from the derived classes.
 */

namespace Autobattle
{
    class Program
    {
        static Random rng = new Random();

        // Return 1 if A wins, -1 if B wins, and 0 if it is a draw.
        static int DoBattle(Character A, Character B)
        {
            int initiativeA = A.RollInitiative(rng);
            int initiativeB = B.RollInitiative(rng);

            bool AFirst;
            if (initiativeA > initiativeB)
                AFirst = true;
            else if (initiativeB > initiativeA)
                AFirst = false;
            else
                AFirst = (rng.Next(0, 2) == 0);

            if (rng.Next(0, 2) == 0)
            {
                A.DoBattleCry(B);
                B.DoBattleCry(A);
            }
            else
            {
                B.DoBattleCry(A);
                A.DoBattleCry(B);
            }

            Console.WriteLine(A.GetName() + " has up to " + A.GetAttackDamage() + " attack damage.");
            Console.WriteLine(B.GetName() + " has up to " + B.GetAttackDamage() + " attack damage.");


            Console.WriteLine("Press Enter to start...");
            Console.ReadLine();

            Console.WriteLine();
            Console.WriteLine("The battle begins!");
            Console.WriteLine();

            int numRounds = 10;
            while (A.GetHitpoints() > 0 && B.GetHitpoints() > 0 && numRounds > 0)
            {
                if (AFirst)
                {
                    A.Act(rng, B);
                    if (B.GetHitpoints() > 0)
                        B.Act(rng, A);
                }
                else
                {
                    B.Act(rng, A);
                    if (A.GetHitpoints() > 0)
                        A.Act(rng, B);
                }
                numRounds--;
            }

            if (numRounds <= 0 || (A.GetHitpoints() < 0 && B.GetHitpoints() < 0))
                return 0;
            else if (B.GetHitpoints() < 0)
                return 1;
            else
                return -1;
        }

        static void Main(string[] args)
        {
            Character myGuy = Character.GetRandom(rng);
            Character yerGuy = Character.GetRandom(rng);

            Console.WriteLine("Battle Result: " + DoBattle(myGuy, yerGuy));
            Console.ReadLine();
        }
    }
}
