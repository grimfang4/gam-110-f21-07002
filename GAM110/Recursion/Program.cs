﻿using System;

namespace Recursion
{
    class Program
    {
        static float Square(float number)
        {
            return number * number;
        }

        static float Distance(float x1, float y1, float x2, float y2)
        {
            return (float)Math.Sqrt(Square(x2 - x1) + Square(y2 - y1));
        }

        /*
         * 1) Recursive call
         * 2) Input change (works toward the base case)
         * 3) Termination condition (base case)
         */
        static void Countdown(int start)
        {
            if (start < 0)
                return;
            Console.WriteLine(start);
            Countdown(start - 1);
        }

        static string GetAlphabet(char letter)
        {
            if(letter == 'z')
            {
                return letter.ToString();
            }
            return letter.ToString() + GetAlphabet((char)(letter + 1));
        }

        static string GetAlphabet()
        {
            return GetAlphabet('a');
        }



        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Countdown(6);

            string alphabet = GetAlphabet();

            Console.WriteLine(alphabet);

            Console.ReadLine();
        }
    }
}
