﻿using System;

namespace FunctionReview
{
    class Program
    {
        // Function definition
        static void PrintThisManyTimes(string message, int numTimes)
        {
            for (int i = 0; i < numTimes; ++i)
            {
                Console.WriteLine(message);
            }
        }


        static int GetNextOdd(int n)
        {
            if (n % 2 == 0)
            {
                int answer = n + 1;
                return answer;
            }
            else
                return n + 2;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            // Function call / invocation
            PrintThisManyTimes("This is a message!", 6);

            Console.WriteLine("Next odd after 4: " + GetNextOdd(4));
            Console.WriteLine("Next odd after 7: " + GetNextOdd(7));


            Console.ReadLine();
        }
    }
}
