﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MazeGame
{
    class Player
    {
        private int x;
        private int y;
        private char avatar;
        private char currentTile;

        public Player(Maze maze)
        {
            avatar = '@';
            x = -1;
            y = -1;

            int startX = maze.GetWidth() / 2;
            int startY = maze.GetHeight() / 2;

            // Locate the 'S' and start the player there.
            for(int j = 0; j < maze.GetHeight(); ++j)
            {
                for(int i = 0; i < maze.GetWidth(); ++i)
                {
                    if(maze.GetTile(i, j) == 'S')
                    {
                        startX = i;
                        startY = j;
                    }
                }
            }


            currentTile = maze.GetTile(startX, startY);
            MoveTo(startX, startY, maze);
        }

        public void MoveTo(int newX, int newY, Maze maze)
        {
            if (newX < 0 || newX >= maze.GetWidth() || newY < 0 || newY >= maze.GetHeight())
                return;

            // Get what is at the new tile
            char newTile = maze.GetTile(newX, newY);
            if (newTile == '#')
                return;

            // Replace our old tile
            maze.SetTile(x, y, currentTile);
            currentTile = newTile;

            x = newX;
            y = newY;

            maze.SetTile(x, y, avatar);
        }

        public void MoveRight(Maze maze)
        {
            MoveTo(x+1, y, maze);
        }

        public void MoveUp(Maze maze)
        {
            MoveTo(x, y-1, maze);
        }

        public void MoveLeft(Maze maze)
        {
            MoveTo(x - 1, y, maze);
        }

        public void MoveDown(Maze maze)
        {
            MoveTo(x, y + 1, maze);
        }
    }
}
