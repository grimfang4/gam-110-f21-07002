﻿using System;

namespace MazeGame
{
    class Program
    {
        static void ExploreUNICODE()
        {
            char c = (char)9794;
            //char c = '\u263B';

            bool done = false;
            while (!done)
            {
                for (int i = 0; i < 50; ++i)
                {
                    Console.Write((char)(c + (char)i));
                }
                Console.WriteLine();

                // Wait for user to press a key
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.RightArrow)
                    c += (char)50;
                else if (keyInfo.Key == ConsoleKey.LeftArrow)
                    c -= (char)50;
                else if (keyInfo.Key == ConsoleKey.Spacebar)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    for (int i = 0; i < 50; ++i)
                    {
                        char newC = (char)(c + (char)i);
                        string hex = string.Format("{0:X}", (int)newC);
                        Console.WriteLine("\nThis character: " + newC + " is at value " + hex);
                    }
                    Console.ForegroundColor = ConsoleColor.Gray;
                }
            }
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.CursorVisible = false;

            //ExploreUNICODE();

            Console.WriteLine("Hello World!");

            Maze maze = new Maze();
            Player player = new Player(maze);

            bool done = false;
            while (!done)
            {
                // Redraw the maze
                Console.SetCursorPosition(0, 0);
                maze.Draw();

                // Wait for user to press a key
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.RightArrow)
                    player.MoveRight(maze);
                else if (keyInfo.Key == ConsoleKey.UpArrow)
                    player.MoveUp(maze);
                else if (keyInfo.Key == ConsoleKey.LeftArrow)
                    player.MoveLeft(maze);
                else if (keyInfo.Key == ConsoleKey.DownArrow)
                    player.MoveDown(maze);
            }
        }
    }
}
