﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MazeGame
{
    class Maze
    {
        private char[,] mazeData;

        public Maze()
        {
            mazeData = new char[10, 10] {
                {'#', '#', '#', '#', '#', '#', '#', '#', '#', '#'},
                {'#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'},
                {'#', ' ', ' ', ' ', ' ', '#', '#', '#', '#', '#'},
                {'#', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#'},
                {'#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'},
                {'#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'},
                {'#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'},
                {'#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'},
                {'#', 'S', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'},
                {'#', '#', '#', '#', '#', '#', '#', '#', '#', '#'}
            };
        }

        public void Draw()
        {
            for (int row = 0; row < mazeData.GetLength(0); ++row)
            {
                for (int col = 0; col < mazeData.GetLength(1); ++col)
                {
                    Console.Write(mazeData[row, col]);
                    Console.Write(' ');
                }
                Console.WriteLine();
            }
        }

        public int GetWidth()
        {
            return mazeData.GetLength(1);  // num columns
        }

        public int GetHeight()
        {
            return mazeData.GetLength(0);  // num rows
        }

        public void SetTile(int x, int y, char c)
        {
            if (x < 0 || x >= GetWidth() || y < 0 || y >= GetHeight())
                return;

            mazeData[y, x] = c;
        }

        public char GetTile(int x, int y)
        {
            return mazeData[y, x];
        }

    }
}
