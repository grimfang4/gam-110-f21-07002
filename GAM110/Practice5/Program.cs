﻿using System;

namespace Practice5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");


            // Ask the user for width and height, then draw a rectangle out of asterisks.

            Console.WriteLine("Give me the width: ");

            string widthString = Console.ReadLine();

            Console.WriteLine("Give me the height: ");

            string heightString = Console.ReadLine();

            int w, h;

            if (int.TryParse(widthString, out w))
            {
                if (int.TryParse(heightString, out h))
                {

                    for (int j = 0; j < h; ++j)
                    {
                        for (int i = 0; i < w; ++i)
                        {
                            Console.Write("*");
                        }
                        Console.WriteLine();
                    }


                }
                else
                    Console.WriteLine("Invalid height");
            }
            else
                Console.WriteLine("Invalid width");

            Console.ReadLine();

        }
    }
}
