﻿using System;
using System.Collections.Generic;

namespace Generic_Containers
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[] array = {67, 23, 234, 778, 39};

            List<int> myList = new List<int>();

            myList.Add(654);
            myList.Add(32222);

            for(int i = 0; i < myList.Count; ++i)
            {
                Console.WriteLine("List element " + i + ": " + myList[i]);
            }

            //myList.AddRange(array);

            Dictionary<string, string> myDictionary = new Dictionary<string, string>();

            myDictionary.Add("cow", "fully grown female animal of a domesticated breed of ox");
            myDictionary.Add("imprecate", "curse or call evil on");
            myDictionary.Add("defenestrate", "throw out the window");
            myDictionary.Add("roux", "mix of butter and flour");

            Console.WriteLine("A cow definition: " + myDictionary["cow"]);

            Console.WriteLine("Give me a sentence!");
            string input = Console.ReadLine();

            string[] words = input.Split(' ', '.');
            for(int i = 0; i < words.Length; ++i)
            {
                string word = words[i];
                if (myDictionary.ContainsKey(word.ToLower()))
                    word = myDictionary[word];

                Console.Write(word + " ");
            }



            Console.ReadLine();
        }
    }
}
