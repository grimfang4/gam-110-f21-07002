﻿using System;
using System.Diagnostics;

/*
 * 
 * Make a program where the user answers a bunch of math problems while being timed.
 * 
 * Count the right and wrong answers
 * Get input from the player and compare to the answer
 * Keep track of the time for the whole thing
 * Loop through ten questions and generate stuff (numbers and operator)
 * 
 */

namespace SpeedMath
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Speed Math!");

            int right = 0;
            int wrong = 0;

            Stopwatch sw = new Stopwatch();

            sw.Start();

            Random rng = new Random();
            int numQuestions = 10; //rng.Next(1, 1000000);

            for (int i = 0; i < numQuestions; ++i)
            {
                int x = rng.Next(0, 10);
                int y = rng.Next(1, 10);

                int answer = 0;
                string question = string.Empty;

                int operatorNum = rng.Next(0, 4);
                switch(operatorNum)
                {
                    case 0:  // addition
                        answer = x + y;
                        question = x + " + " + y;
                        break;
                    case 1:  // subtraction
                        answer = x - y;
                        question = x + " - " + y;
                        break;
                    case 2:  // multiplication
                        answer = x * y;
                        question = x + " * " + y;
                        break;
                    case 3:  // division
                        answer = x / y;
                        question = x + " / " + y;
                        break;
                }


                Console.Write(question + " = ");
                string response = Console.ReadLine();
                int guess;

                if (int.TryParse(response, out guess))
                {
                    if (guess == answer)
                        right++;
                    else
                        wrong++;
                }
                else
                    wrong++;
            }

            sw.Stop();

            Console.WriteLine("You got " + right + " out of " + numQuestions + " correct!");
            Console.WriteLine("You took " + sw.Elapsed);


            Console.ReadLine();
        }
    }
}
