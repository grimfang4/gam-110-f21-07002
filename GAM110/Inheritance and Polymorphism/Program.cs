﻿using System;
using System.Collections.Generic;

namespace Inheritance_and_Polymorphism
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Animal myAnimal = new Giraffe();
            myAnimal.Speak();

            Dog fido = new Dog();
            fido.Bark();
            fido.Speak();

            // Reference types
            Dog max = new Dog();
            Dog ben = fido;

            ben.SetNumLegs(3);


            // POLYMORPHISM
            Animal someAnimal = fido;
            someAnimal.Speak();

            List<Animal> animals = new List<Animal>();
            animals.Add(fido);
            animals.Add(max);
            animals.Add(myAnimal);
            animals.Add(new Giraffe());

            Console.WriteLine("START TALKING, ANIMALS!");
            for(int i = 0; i < animals.Count; ++i)
            {
                animals[i].Speak();
            }




            Console.ReadLine();
        }
    }
}
