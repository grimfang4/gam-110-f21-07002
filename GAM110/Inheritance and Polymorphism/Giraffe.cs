﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inheritance_and_Polymorphism
{
    class Giraffe : Animal
    {
        public override void Speak()
        {
            Console.WriteLine("Ringdingding");
        }
    }
}
