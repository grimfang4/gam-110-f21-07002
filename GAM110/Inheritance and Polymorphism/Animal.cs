﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inheritance_and_Polymorphism
{
    abstract class Animal
    {
        private float friendliness;
        private int numLegs;

        public int numEyes { get; set; }

        public Animal()
        {
            numLegs = 4;
            friendliness = 0.5f;
        }

        public int GetNumLegs()
        {
            return numLegs;
        }

        public void SetNumLegs(int n)
        {
            numLegs = n;
        }

        public float GetFriendliness()
        {
            return friendliness;
        }

        public abstract void Speak();

        // Eat, Attack, Speak
    }
}
