﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inheritance_and_Polymorphism
{
    class Dog : Animal
    {
        private int smellPower = 1;

        public Dog()
        {

        }

        public override void Speak()
        {
            Bark();
        }

        public void Bark()
        {
            Console.WriteLine("WOOOOOOF!");
        }
    }
}
