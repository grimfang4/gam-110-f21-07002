﻿using System;

namespace CarrotEconomy
{
    class Program
    {
        public static string[] descriptors = {"Strength", "Agility", "Dangerous"};

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Random rng = new Random();

            Carrot c = new Carrot(rng);
            Console.WriteLine("Got a carrot: " + c.GetName());

            Hero hero = new Hero();

            CarrotStore store1 = new CarrotStore();
            CarrotStore store2 = new CarrotStore();
            CarrotStore store3 = new CarrotStore();

            // CarrotStore[] stores = {new CarroteStore(), new CarroteStore(), new CarroteStore()};

            bool gameOver = false;
            while(!gameOver)
            {
                Console.Clear();
                Console.WriteLine("Which store do you want to visit?");
                Console.WriteLine("1) " + store1.GetName());
                Console.WriteLine("2) " + store2.GetName());
                Console.WriteLine("3) " + store3.GetName());
                Console.WriteLine("4) Quit");

                string response = Console.ReadLine();
                int choice;
                if (!int.TryParse(response, out choice))
                    choice = -1;

                switch(choice)
                {
                    case 1:
                        // TODO: Go shopping!
                        break;
                    case 2:
                        // TODO: Go shopping!
                        break;
                    case 3:
                        // TODO: Go shopping!
                        break;
                    case 4:
                        gameOver = true;
                        break;
                    default:
                        Console.WriteLine("That is an invalid choice!  Press Enter to continue...");
                        Console.ReadLine();
                        break;
                }
            }

            Console.ReadLine();
        }
    }
}
