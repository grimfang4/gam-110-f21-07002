﻿using System;
using System.Collections.Generic;
using System.Text;

/*
Write the Carrot class

The Carrot class stores an amount of power and a string that contains a word describing
the Carrot. Carrots can tell you the amount of power and their full name, e.g. "Strength Carrot".

Carrot class:
    ● Integer member variable for the amount of power
    ● String member variable for the descriptor
    ● Constructor that initializes the Carrot
        ○ Random power initialized in constructor, between 2 and 5
        ○ Random descriptor from array
    ● Public accessor function, GetName()
    ● Public accessor function, GetPower()
*/

namespace CarrotEconomy
{
    class Carrot
    {
        private int power;
        private string descriptor;

        public Carrot(Random rng)
        {
            power = rng.Next(2, 6);  // 2-5

            descriptor = Program.descriptors[rng.Next(0, Program.descriptors.Length)];
        }

        public string GetName()
        {
            return descriptor + " Carrot";
        }

        public int GetPower()
        {
            return power;
        }

    }
}
