﻿using System;

namespace QuinnProblem
{
    class Program
    {
        //Given a string, print out the frequency of each letter (case insensitive).
        static void Main(string[] args)
        {
            Console.WriteLine("Hey gimme a string!");
            string input = Console.ReadLine();
            input = input.ToLower();
            int[] letterCounter = new int[26];

            for (int i = 0; i < input.Length; ++i)
            {
                ++letterCounter[input[i] - 97];
            }

            for (int i = 0; i < letterCounter.Length; ++i)
            {
                Console.WriteLine((char)(i + 97) + ": " + letterCounter[i]);
            }

        }
    }
}
