﻿using System;

namespace InsertionSort
{
    class Program
    {
        static void InsertionSort(int[] values)
        {
            int currentIndex = 1;

            while(currentIndex != values.Length)
            {
                int swapIndex = currentIndex;
                while(swapIndex > 0 && values[swapIndex] < values[swapIndex - 1])
                {
                    // Do a swap!
                    int temp = values[swapIndex];
                    values[swapIndex] = values[swapIndex - 1];
                    values[swapIndex - 1] = temp;

                    swapIndex--;
                }

                currentIndex++;
            }
        }

        static void PrintArray(int[] values)
        {
            for (int i = 0; i < values.Length; ++i)
            {
                Console.Write(values[i] + " ");
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[] values = new int[10];

            Random rng = new Random();

            for(int i = 0; i < values.Length; ++i)
            {
                values[i] = rng.Next(1, 21);
            }

            PrintArray(values);

            InsertionSort(values);

            PrintArray(values);

            Console.ReadLine();
        }
    }
}
