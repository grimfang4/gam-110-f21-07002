﻿/*
Your tasks:
1) Use the debugger to fix bugs
2) Leave a comment describing the bug and the fix

Look at the Main() function.  Each section of code (most commented out right now) performs a specific task incorrectly.
However, the code in the Main() function is correct.  You must only edit the other functions and NOT main().
Speaking of which, the testing code in Main() can show you examples of what input and output to expect.

To solve the problems, fix the code in the other functions.  *Do not just rip out the code and replace it all!*
Make the smallest changes you can to fix the problems.
Read the code carefully and fix it to match the expectations in the function name, usage, and comments.

As you solve each problem, uncomment the next function AND uncomment the matching section in Main().
*/


using System;

class Program
{

    static int Return20()
    {
        int x = 2;
        int y = 8;

        return x + y * 2;
    }

    /*static bool IsBetween4and1(int num)
    {
        return num <= 4 && >= 1;
    }*/

    /*static bool IsBetween(int num, int low, int high)
    {
        return num == low && num == high;
    }*/

    // Returns a string of number sequence starting at `from` and ending at `to`.
    static string MakeStringFromTo(int from, int to)
    {
        string result = string.Empty;
		while(from <= to)
		{
			result += from.ToString();
            from++;
        }
		return result;
    }




    static void Main(string[] args)
    {
        // Some simple tests...

        if (Return20() == 20)
            Console.WriteLine("Pass");
        else
            Console.WriteLine("Fail");

        /*if (IsBetween4and1(3) && !IsBetween4and1(0) && !IsBetween4and1(5))
            Console.WriteLine("Pass");
        else
            Console.WriteLine("Fail");*/

        /*if (IsBetween(50, 45, 55) && !IsBetween(0, 5, 10) && IsBetween(0, -10, 1))
            Console.WriteLine("Pass");
        else
            Console.WriteLine("Fail");*/

        if (MakeStringFromTo(1, 3) == "123" && MakeStringFromTo(3, 7) == "34567" && MakeStringFromTo(4, -5) == "")
            Console.WriteLine("Pass");
        else
            Console.WriteLine("Fail");



        Console.WriteLine("Press enter to quit.");
        Console.ReadLine();
    }
}