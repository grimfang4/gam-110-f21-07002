﻿using System;

class HelloWorld
{
    static void Main(string[] args)
    {
        Console.WriteLine("Hello\n World!");
        Console.ReadLine();

        Console.WriteLine("Hello World!");
        Console.WriteLine("Hello World!");
        Console.WriteLine("Hello World!");
        Console.WriteLine("Hello World!");
        Console.ReadLine();
    }
}