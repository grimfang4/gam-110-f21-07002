﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ThisNullAndFileIO
{
    class Hero
    {
        public string name;
        public int strength;
        public int intelligence;
        public int coolness;

        public Hero()
        {
            name = "Jeffkilles";
            strength = 10;
            intelligence = -6;
            coolness = 99;

            Program.lastHero = this;
        }

        public Hero(string newName, int strength, int intelligence)
        {
            name = newName;
            this.strength = strength;
            this.intelligence = intelligence;
            coolness = 99;

            Program.lastHero = this;
        }

        public void Save(string filename)
        {
            StreamWriter writer = new StreamWriter(filename);

            writer.WriteLine(name);
            writer.WriteLine(strength);
            writer.WriteLine(intelligence);
            writer.WriteLine(coolness);

            writer.Close();
        }

        public void Load(string filename)
        {
            StreamReader reader = new StreamReader(filename);

            name = reader.ReadLine();
            strength = int.Parse(reader.ReadLine());
            intelligence = int.Parse(reader.ReadLine());
            coolness = int.Parse(reader.ReadLine());

            while(reader.Peek() > -1)
            {
                string s = reader.ReadLine();
                Console.WriteLine("Item: " + s);
            }

            reader.Close();
        }

    }
}
