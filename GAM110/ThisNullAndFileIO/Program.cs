﻿using System;

namespace ThisNullAndFileIO
{
    class Program
    {
        public static Hero lastHero = null;

        public static int numHeroesLeft = 3;
        public static Hero MakeHero()
        {
            if (numHeroesLeft <= 0)
                return null;

            numHeroesLeft--;
            return new Hero();
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            if(lastHero != null)
                Console.WriteLine("Last hero's strength: " + lastHero.strength);

            Hero myHero = new Hero();

            myHero.Load("save1.txt");

            Console.WriteLine("Hero name: " + myHero.name);
            //myHero.Save("save1.txt");


            while (true)
            {
                Console.WriteLine("Press Enter to make a new hero.");
                Console.ReadLine();

                Hero h = MakeHero();
                if (h == null)
                {
                    Console.WriteLine("No More Heroes!");
                    break;
                }
            }

            Console.ReadLine();
        }
    }
}
