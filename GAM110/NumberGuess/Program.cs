﻿using System;

class Program{
    static void Main(string[] args){
        //done right up front; exactly once

        Random rng;
        rng = new Random();

        // Ranges in math class
        // (0, 10) - Exclusive
        // (0, 10] - Inclusive lower bound, exclusive upper bound
        // [0, 10] - Inclusive bounds

        int answer = rng.Next(1, 101);  // "exclusive" upper bound
        int guess = 0; //I'm up here
        string aStringSure;

        //Tell user "I'm thinking of a number between 1-100, start guessing"
        Console.WriteLine("I'm thinking of a number between 1-100, start guessing");

        //a While loop!
        while(guess != answer){
            //I get repeated until guess == answer!
            //Red line and then ocnvert to number
            aStringSure = Console.ReadLine();
            //Conver to int time? yes -> int.parse
            guess = int.Parse(aStringSure); //thankyou <3
            //if guess is greater than answer, lower, if guess lower, higher, if guess correct, congratulations
            //a condition!
            
            if(guess > answer){
                Console.WriteLine("Lower"); //print lower!
            }else if(guess < answer){
                Console.WriteLine("Higher");
            }else {
                Console.WriteLine("W00000000000000000000000000000000");
            }
        }
    }
}
/* Game loop!
 * "You've won" win condition
 *      check if number is corect
 * Input
 * Try again Error
 *      Number doesn't equal the answer
 *              Higher
 *              Lower
 * If(integer is greater than x)
 * Numbers 7 (that's the answer)
 *      Random???
 *          We'll see
 * Pizza
 */