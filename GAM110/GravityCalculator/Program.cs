﻿using System;

namespace GravityCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Planet planetA = new Planet();
            planetA.mass = 5.97e24;
            // TODO: Set the position

            Planet planetB = new Planet();
            planetB.mass = 6.4e22;
            // TODO: Set the position


            // TODO: Calculate gravitational force between the planets

            // Printing in scientific notation
            string info1 = string.Format("{0:g}", planetA.mass);
            string info2 = string.Format("{0:e}", planetA.mass);
            Console.WriteLine("SciNot: " + info1 + " vs " + info2);
            Console.ReadLine();
        }
    }
}
