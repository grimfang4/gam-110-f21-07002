﻿using System;

class Program
{
    static void Main(string[] args)
    {
        bool gameover = false;
        while (!gameover)
        {

            bool badInput = true;
            while (badInput)
            {
                Console.Clear();
                Console.WriteLine("What do you want to do?");
                Console.WriteLine("1) Attack\n2) Defend\n3) Run\n4) Quit");
                string input = Console.ReadLine();
                int inputInteger;
                if (int.TryParse(input, out inputInteger))
                {

                    badInput = false;
                    if (inputInteger == 1)
                    {
                        // do attack
                        Console.WriteLine("Attack!!");
                    }
                    else if (inputInteger == 2)
                    {
                        // do defend
                        Console.WriteLine("Defend!!");
                    }
                    else if (inputInteger == 3)
                    {
                        // do run
                        Console.WriteLine("Run!!");
                    }
                    else if (inputInteger == 4)
                    {
                        // quit
                        Console.WriteLine("Quit!!");
                        gameover = true;
                    }
                    else
                    {
                        Console.WriteLine("That is not an option!!!  Try again, please.  Please press Enter to continue...");
                        badInput = true;
                    }
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("You need to type a number!  Please press Enter to continue...");
                    badInput = true;
                    Console.ReadLine();
                }
            }
        }


    }
}
