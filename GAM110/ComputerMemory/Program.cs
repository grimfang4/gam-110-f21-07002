﻿using System;

namespace ComputerMemory
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");


            // 1 byte is 8 bits
            // 00111011

            // int - 4 bytes, 32 bits (range roughly -2,000,000,000 to 2,000,000,000)
            // 00100111 11001111 01011011 00100010

            // uint (unsigned int) - 4 bytes, 32 bits (roughly 0 to 4,000,000,000)

            // long - 8 bytes, 64 bits

            // short - 2 bytes

            // bool
            Console.WriteLine("Size of an int: " + sizeof(int));
            Console.WriteLine("Range of an int: " + int.MinValue + " to " + int.MaxValue);
            Console.WriteLine("Size of a bool: " + sizeof(bool));

            Console.ReadLine();

            // float - 4 bytes, decimal value
            float myFloat = 5.567893f;

            // double - 8 bytes, decimal value

            // char



            // string


            // arrays

            int gradeForPreston = 69;
            int gradeForJason = 90;

            float average1 = (gradeForPreston + gradeForJason) / 2;


            //int[] grades = {69, 90, 78, 58, 99, 100, 85, 89, 67, 89, 100};
            int[] grades = new int[20];

            for(int i = 0; i < grades.Length; ++i)
            {
                grades[i] = i + 60;
            }

            int sum = 0;
            for(int i = 0; i < grades.Length; ++i)
            {
                sum += grades[i];
            }

            float average2 = sum / (float)grades.Length;


            // enum
            // half
        }
    }
}
