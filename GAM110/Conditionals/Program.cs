﻿using System;

class Conditionals
{
    public static void Main(string[] args)
    {
        int a;
        a = 5 + 7;

        if(a < 20)
        {
            Console.WriteLine("Hey, that value is less than 20!");
        }

        if(a >= 50)
        {
            Console.WriteLine("That number is big!");
        }
        else
        {
            Console.WriteLine("Okay, the number is not greater than or equal to 50...");
        }

        string name = "Jon";

        if(name == "Jon")
        {
            // TODO: Say nice things
        }

        if(name != "Jon")
        {
            // TODO: Say nasty things
        }

        if(!(name == "Jon"))
        {
            // Same as not-equals Jon
        }


        Console.ReadLine();
    }
}